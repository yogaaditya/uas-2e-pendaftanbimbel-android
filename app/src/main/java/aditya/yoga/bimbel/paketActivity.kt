package aditya.yoga.bimbel

import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.paket.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap

class paketActivity : AppCompatActivity() , View.OnClickListener {

    lateinit var adapPaket: AdapterPaket
    var id_paket : String =""
    var daftarpaket = mutableListOf<HashMap<String,String>>()
    var uri1 = "http://192.168.43.36/bimbel/show_paket.php"
    var uri2 = "http://192.168.43.36/bimbel/query_paket.php"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.paket)
        adapPaket =  AdapterPaket(daftarpaket,this)
        rv2.layoutManager = LinearLayoutManager(this)
        rv2.adapter = adapPaket

        btInsPaket.setOnClickListener(this)
        btUpdtPaket.setOnClickListener(this)
        btDelPaket.setOnClickListener(this)
        btBckP.setOnClickListener(this)

    }

    override fun onStart() {
        super.onStart()
        Showpaket()
    }


    fun Showpaket(){
        val request = StringRequest(Request.Method.POST,uri1, Response.Listener { response ->
            daftarpaket.clear()
            adapPaket.notifyDataSetChanged()
            val jsonArray = JSONArray(response)
            for (x in 0..(jsonArray.length()-1)){
                val jsonObject = jsonArray.getJSONObject(x)
                var paket1 = HashMap<String,String>()
                paket1.put("nama_paket",jsonObject.getString("nama_paket"))
                paket1.put("harga_paket",jsonObject.getString("harga_paket"))
                paket1.put("id_paket",jsonObject.getInt("id_paket").toString())
                daftarpaket.add(paket1)

            }
            adapPaket.notifyDataSetChanged()

        }, Response.ErrorListener { error ->
            Toast.makeText(this,"Kesalahan Saat Konfigurasi", Toast.LENGTH_SHORT).show()
        })
        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }



    fun query(mode : String){
        val request = object : StringRequest(Method.POST,uri2,
            Response.Listener { response ->

                val jsonobject  = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil ", Toast.LENGTH_LONG).show()
                    Showpaket()
                }else{
                    Toast.makeText(this,"Operasi Gagal ", Toast.LENGTH_LONG).show()
                } },
            Response.ErrorListener { error ->
            })
        {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("nama_paket",edPaket.text.toString())
                        hm.put("harga_paket",edHarga.text.toString())
                    }

                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("id_paket",id_paket)
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("nama_paket",edPaket.text.toString())
                        hm.put("harga_paket",edHarga.text.toString())
                        hm.put("id_paket",id_paket)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btInsPaket->{
                query("insert")



            }
            R.id.btDelPaket->{
                query("delete")


            }
            R.id.btUpdtPaket->{
                query("update")
            }
            R.id.btBckP->{
                val intent = Intent(this,MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }



}