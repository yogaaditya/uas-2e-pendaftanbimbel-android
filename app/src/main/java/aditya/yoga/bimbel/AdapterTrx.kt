package aditya.yoga.bimbel

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_data_trx.*
import kotlinx.android.synthetic.main.transaksi.*


class AdapterTrx(val dataTrx: List<HashMap<String,String>>, val trxActivity : transaksiActivity): RecyclerView.Adapter<AdapterTrx.HolderTrx> (){
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterTrx.HolderTrx {
        val v  =LayoutInflater.from(parent.context).inflate(R.layout.item_data_trx,parent,false)
        return HolderTrx(v)
    }

    override fun getItemCount(): Int {
        return dataTrx.size
    }

    override fun onBindViewHolder(holder: AdapterTrx.HolderTrx, position: Int) {
        val data =dataTrx.get(position)
        holder.txIdTrx.setText(data.get("id_trx"))
        holder.txnama.setText(data.get("id_user"))
        holder.txNmPk.setText(data.get("id_paket"))
        holder.txTglTrx.setText(data.get("tanggal"))
        holder.txHargaPk.setText(data.get("total"))

        if (position.rem(2) == 0) holder.layouts.setBackgroundColor(Color.rgb(230,245,240))
        else holder.layouts.setBackgroundColor(Color.rgb(255,255,245))

        holder.layouts.setOnClickListener(View.OnClickListener{
            val pos = trxActivity.daftarPkt.indexOf(data.get("id_paket"))
            val pos1 = trxActivity.daftarMrd1.indexOf(data.get("id_user"))
            trxActivity.spMurid.setSelection(pos)
            trxActivity.spPaket.setSelection(pos1)
            trxActivity.edTgl.setText(data.get("tgl"))
            trxActivity.edTotalHrg.setText(data.get("total"))

        })
    }

    class HolderTrx(v : View) :  RecyclerView.ViewHolder(v){
        val txIdTrx = v.findViewById<TextView>(R.id.txIdTrx)
        val txnama = v.findViewById<TextView>(R.id.txNamaMur)
        val txTglTrx = v.findViewById<TextView>(R.id.txTglTrx)
        val txNmPk = v.findViewById<TextView>(R.id.txNmPk)
        val txHargaPk = v.findViewById<TextView>(R.id.txHargaPk)
        val layouts = v.findViewById<ConstraintLayout>(R.id.trxLayout)
    }

}