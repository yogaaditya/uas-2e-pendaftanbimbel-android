package aditya.yoga.bimbel

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.setting.*

class setting : AppCompatActivity(), View.OnClickListener {
    lateinit var pref : SharedPreferences
    var setwar =""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.setting)
        pref = getSharedPreferences("a", Context.MODE_PRIVATE)
        setwar = pref.getString("bl","").toString()

        btnColor1.setOnClickListener(this)
        btnColor2.setOnClickListener(this)
        btnColor3.setOnClickListener(this)
        btnColor4.setOnClickListener(this)
        warna()
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnColor1->{
                pref =getSharedPreferences("a", Context.MODE_PRIVATE)
                val warna = pref.edit()
                warna.putString("bl","biru")
                warna.commit()
                Toast.makeText(this," warna berhasil di ubah",Toast.LENGTH_LONG).show()
                val asa  = Intent(this,MainActivity::class.java)
                startActivity(asa)
            }
            R.id.btnColor2->{
                pref =getSharedPreferences("a",Context.MODE_PRIVATE)
                val warna = pref.edit()
                warna.putString("bl","hijau")
                warna.commit()
                Toast.makeText(this," warna berhasil di ubah",Toast.LENGTH_LONG).show()
                val asa  = Intent(this,MainActivity::class.java)
                startActivity(asa)
            }
            R.id.btnColor3->{
                pref =getSharedPreferences("a",Context.MODE_PRIVATE)
                val warna = pref.edit()
                warna.putString("bl","putih")
                warna.commit()
                Toast.makeText(this," warna berhasil di ubah",Toast.LENGTH_LONG).show()
                val asa  = Intent(this,MainActivity::class.java)
                startActivity(asa)
            }
            R.id.btnColor4->{
                pref =getSharedPreferences("a",Context.MODE_PRIVATE)
                val warna = pref.edit()
                warna.putString("bl","kuning")
                warna.commit()
                Toast.makeText(this," warna berhasil di ubah",Toast.LENGTH_LONG).show()
                val asa  = Intent(this,MainActivity::class.java)
                startActivity(asa)
            }

        }
    }

    @SuppressLint("ResourceAsColor")
    fun  warna(){
        if(setwar=="biru"){
            saya.setBackgroundColor(Color.BLUE)
        }
        else if(setwar=="kuning"){
            saya.setBackgroundColor(R.color.colorPrimary)
        }
        else if(setwar=="hijau"){
            saya.setBackgroundColor(Color.GREEN)
        }
        else if(setwar=="putih"){
            saya.setBackgroundColor(Color.WHITE)
        }


    }


}