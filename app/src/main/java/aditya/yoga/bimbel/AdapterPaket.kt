package aditya.yoga.bimbel

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_data_paket.*
import kotlinx.android.synthetic.main.paket.*

class AdapterPaket(val datapaket : List<HashMap<String,String>>,val pkt : paketActivity) : RecyclerView.Adapter<AdapterPaket.HolderPaket> () {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterPaket.HolderPaket {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_data_paket,parent,false)
        return HolderPaket(v)
    }

    override fun getItemCount(): Int {
        return datapaket.size
    }

    override fun onBindViewHolder(holder: AdapterPaket.HolderPaket, position: Int) {
        val data =datapaket.get(position)
        holder.txpkt.setText(data.get("nama_paket"))
        holder.txhrg.setText(data.get("harga_paket"))
        if (position.rem(2) == 0) holder.layouts.setBackgroundColor(Color.rgb(230,245,240))
        else holder.layouts.setBackgroundColor(Color.rgb(255,255,245))

        holder.layouts.setOnClickListener(View.OnClickListener {
            pkt.edPaket.setText(data.get("nama_paket"))
            pkt.edHarga.setText(data.get("harga_paket"))
            pkt.id_paket = data.get("id_paket").toString()

        })


    }

    class HolderPaket(v : View) :  RecyclerView.ViewHolder(v){
        //  val prod_id = v.findViewById<TextView>(R.id.pro_id)
        val txpkt = v.findViewById<TextView>(R.id.txNamaPk)
        val txhrg = v.findViewById<TextView>(R.id.txHargaPk)
        val layouts = v.findViewById<ConstraintLayout>(R.id.paketLayout)
    }

}