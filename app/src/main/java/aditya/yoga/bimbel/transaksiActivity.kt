package aditya.yoga.bimbel

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.icu.text.DecimalFormat
import android.icu.text.SimpleDateFormat
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.MotionEvent
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.zxing.integration.android.IntentIntegrator
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.paket.*
import kotlinx.android.synthetic.main.transaksi.*
import kotlinx.android.synthetic.main.user_main.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap

class transaksiActivity : AppCompatActivity(), View.OnClickListener {

    var daftarTrx = mutableListOf<HashMap<String,String>>()
    var daftarMrd1 = mutableListOf<String>()
    var daftarPkt = mutableListOf<String>()
    var pilihPkt = ""
    var pilihMrd1 =""
    var x : Double = 0.0
    var y : Double = 0.0
    var hasil : Double = 0.0
    var tahun = 0
    var bulan = 0
    var tanggal = 0
    lateinit var AdapterTrx : AdapterTrx
    lateinit var adapPkt : ArrayAdapter<String>
    lateinit var adapMrd1 : ArrayAdapter<String>
    var uri = "http://192.168.43.36/bimbel/show_data.php"
    var uri1 = "http://192.168.43.36/bimbel/show_paket.php"
    var uri2 = "http://192.168.43.36/bimbel/show_trx.php"
    var  uri3 ="http://192.168.43.36/bimbel/query_trx.php"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.transaksi)
        val cal : Calendar = Calendar.getInstance()
        bulan = cal.get(Calendar.MONTH)+1
        tanggal = cal.get(Calendar.DAY_OF_MONTH)
        tahun = cal.get(Calendar.YEAR)

        edTgl.setText("$tahun-$bulan-$tanggal")
        intentIntegrator = IntentIntegrator(this)
        AdapterTrx = AdapterTrx(daftarTrx, this)
        rv3.layoutManager = LinearLayoutManager(this)
        rv3.adapter = AdapterTrx
        adapMrd1 = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, daftarMrd1)
        adapPkt = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, daftarPkt)
        spMurid.adapter = adapMrd1
        spMurid.onItemSelectedListener = itemSelect
        spPaket.adapter = adapPkt
        spPaket.onItemSelectedListener = itemSelect1

        btScanQR.setOnClickListener(this)
        btInsTrx.setOnClickListener(this)
        btBckT.setOnClickListener(this)
        btnkali.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        getPaket()
        getMrd1()
        ShowTrx()
    }

    //nampil prodi
    fun getMrd1(){
        val request  = StringRequest(Request.Method.POST,uri,
            Response.Listener { response ->
                daftarMrd1.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarMrd1.add(jsonObject.getString("id_user"))
                }
                adapMrd1.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->

            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    fun getPaket(){
        val request  = StringRequest(Request.Method.POST,uri1,
            Response.Listener { response ->
                daftarPkt.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarPkt.add(jsonObject.getString("id_paket"))
                }
                adapPkt.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->

            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    val itemSelect = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spMurid.setSelection(0)
            pilihMrd1 = daftarMrd1.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihMrd1 = daftarMrd1.get(position)
        }

    }
    val itemSelect1 = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spPaket.setSelection(0)
            pilihPkt = daftarPkt.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihPkt = daftarPkt.get(position)
        }

    }

    fun ShowTrx(){
        val request = StringRequest(Request.Method.POST,uri2,Response.Listener { response ->
            daftarTrx.clear()
            val jsonArray = JSONArray(response)

            for (x in 0..(jsonArray.length()-1)){
                val jsonObject = jsonArray.getJSONObject(x)
                var trx = HashMap<String,String>()
                trx.put("id_trx",jsonObject.getInt("id_trx").toString())
                trx.put("id_user",jsonObject.getInt("id_user").toString())
                trx.put("id_paket",jsonObject.getInt("id_paket").toString())
                trx.put("tanggal",jsonObject.getString("tanggal"))
                trx.put("total",jsonObject.getString("total"))
                daftarTrx.add(trx)
            }
            AdapterTrx.notifyDataSetChanged()
        },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Kesalahan Saat Konfigurasi",Toast.LENGTH_SHORT).show()
            })

        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }

    lateinit var intentIntegrator: IntentIntegrator
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (intentResult != null) {
            if (intentResult.contents !=null) {
                edDiskon.setText(intentResult.contents)
            } else {
                Toast.makeText(this,"Dibatalkan",Toast.LENGTH_SHORT).show()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btInsTrx->{
                query("insert")
            }
            R.id.btScanQR->{
                intentIntegrator.setBeepEnabled(true).initiateScan()
            }
            R.id.btBckT->{
                val intent = Intent(this,MainActivity::class.java)
                startActivity(intent)
                finish()
            }
            R.id.btnkali->{
                var aa = edDiskon.text.toString().toDouble()
                var bb = edHarga.text.toString().toDouble()
                hasil = bb*aa
                edTotalHrg.setText(DecimalFormat("#.##").format(hasil))
            }
        }
    }

    fun query(mode : String){
        val request = object : StringRequest(Method.POST,uri3,
            Response.Listener { response ->
                val jsonobject  = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil ", Toast.LENGTH_LONG).show()
                    ShowTrx()
                }else{
                    Toast.makeText(this,"Operasi Gagal ", Toast.LENGTH_LONG).show()
                } },
            Response.ErrorListener { error ->
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("tgl",edTgl.text.toString())
                        hm.put("total",edTotalHrg.text.toString())
                        hm.put("nama_paket",pilihPkt)
                        hm.put("nama",pilihMrd1)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}