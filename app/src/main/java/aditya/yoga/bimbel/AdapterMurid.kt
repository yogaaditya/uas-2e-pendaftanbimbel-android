package aditya.yoga.bimbel

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.murid.*


class AdapterMurid(val dataMurid: List<HashMap<String,String>>, val murd1: muridActivity): RecyclerView.Adapter<AdapterMurid.HolderDataMurid> (){
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterMurid.HolderDataMurid {
        val v  =LayoutInflater.from(parent.context).inflate(R.layout.item_data_murid,parent,false)
        return HolderDataMurid(v)
    }

    override fun getItemCount(): Int {
        return dataMurid.size
    }

    override fun onBindViewHolder(holder: AdapterMurid.HolderDataMurid, position: Int) {
        val data =dataMurid.get(position)
        holder.nama.setText(data.get("nama"))
        holder.noHp.setText(data.get("no_hp"))
        holder.alamat.setText(data.get("alamat"))
        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(holder.photo)

        if (position.rem(2) == 0) holder.layouts.setBackgroundColor(Color.rgb(230,245,240))
        else holder.layouts.setBackgroundColor(Color.rgb(255,255,245))

        holder.layouts.setOnClickListener(View.OnClickListener{
            murd1.edNamaAd.setText(data.get("nama"))
            murd1.edNoHpAd.setText(data.get("no_hp"))
            murd1.edAlmtAd.setText(data.get("alamat"))
            murd1.id_user = data.get("id_user").toString()
            Picasso.get().load(data.get("url")).into(murd1.img1)

        })
    }

    class HolderDataMurid(v : View) :  RecyclerView.ViewHolder(v){
        val nama = v.findViewById<TextView>(R.id.txNama)
        val noHp = v.findViewById<TextView>(R.id.txNoHp)
        val alamat = v.findViewById<TextView>(R.id.txAlamat)
        val photo = v.findViewById<ImageView>(R.id.img)
        val layouts = v.findViewById<ConstraintLayout>(R.id.muridLayout)
    }

}