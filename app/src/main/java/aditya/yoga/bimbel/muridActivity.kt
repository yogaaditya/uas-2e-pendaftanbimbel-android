package aditya.yoga.bimbel

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.murid.*
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.HashMap

class muridActivity : AppCompatActivity(), View.OnClickListener {

    var daftarmurid = mutableListOf<HashMap<String,String>>()
    var id_user : String =""
    var imStr = ""
    var namafile = ""
    var Fileuri = Uri.parse("")
    lateinit var AdapterMurid : AdapterMurid
    lateinit var  MediaHelperKamera : MediaHelperKamera
    var uri = "http://192.168.43.36/bimbel/show_data.php"
    var uri1 = "http://192.168.43.36/bimbel/query_data.php"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.murid)
        AdapterMurid =  AdapterMurid(daftarmurid,this)
        rv1.layoutManager = LinearLayoutManager(this)
        rv1.adapter = AdapterMurid
        img1.setOnClickListener(this)
        btInsMurid.setOnClickListener(this)
        btUpdtMurid.setOnClickListener(this)
        btDelMurid.setOnClickListener(this)
        btBckM.setOnClickListener(this)

        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        MediaHelperKamera = MediaHelperKamera()
    }

    override fun onStart() {
        super.onStart()
        ShowMurid()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){

            if (requestCode == MediaHelperKamera.getRcCamera()){
                imStr = MediaHelperKamera.getBitmapToString(img1,Fileuri)
                namafile = MediaHelperKamera.getMyfile()
            }
        }
    }

    fun ShowMurid(){
        val request = StringRequest(Request.Method.POST,uri,Response.Listener { response ->
            daftarmurid.clear()
            val jsonArray = JSONArray(response)

            for (x in 0..(jsonArray.length()-1)){
                val jsonObject = jsonArray.getJSONObject(x)
                var murd = HashMap<String,String>()
                murd.put("nama",jsonObject.getString("nama"))
                murd.put("no_hp",jsonObject.getString("no_hp"))
                murd.put("alamat",jsonObject.getString("alamat"))
                murd.put("url",jsonObject.getString("url"))
                murd.put("id_user",jsonObject.getInt("id_user").toString())
                daftarmurid.add(murd)
            }
            AdapterMurid.notifyDataSetChanged()
        },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Kesalahan Saat Konfigurasi",Toast.LENGTH_SHORT).show()
            })

        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.img1->{
                requestPermition()
            }
            R.id.btInsMurid->{
                query("insert")
            }
            R.id.btUpdtMurid->{
                query("update")
            }
            R.id.btDelMurid->{
                query("delete")
            }
            R.id.btBckM ->{
                val intent = Intent(this,MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }

    fun query(mode : String){
        val request = object : StringRequest(Method.POST,uri1,
            Response.Listener { response ->
                val jsonobject  = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil ", Toast.LENGTH_LONG).show()
                    ShowMurid()
                }else{
                    Toast.makeText(this,"Operasi Gagal ", Toast.LENGTH_LONG).show()
                } },
            Response.ErrorListener { error ->
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("nama",edNamaAd.text.toString())
                        hm.put("no_hp",edNoHpAd.text.toString())
                        hm.put("alamat",edAlmtAd.text.toString())
                        hm.put("images",imStr)
                        hm.put("file",namafile)
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("nama",edNamaAd.text.toString())
                        hm.put("no_hp",edNoHpAd.text.toString())
                        hm.put("alamat",edAlmtAd.text.toString())
                        hm.put("images",imStr)
                        hm.put("file",namafile)
                        hm.put("id_user",id_user)
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("id_user",id_user)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun requestPermition() = runWithPermissions(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA){
        Fileuri = MediaHelperKamera.getOutputFileUri()

        val inten  = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        inten.putExtra(MediaStore.EXTRA_OUTPUT,Fileuri)
        startActivityForResult(inten,MediaHelperKamera.getRcCamera())
    }
}