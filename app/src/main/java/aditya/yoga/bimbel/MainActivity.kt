package aditya.yoga.bimbel

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var pref : SharedPreferences
    var warnakat = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        pref = getSharedPreferences("a", Context.MODE_PRIVATE)
        warnakat = pref.getString("bl","").toString()
        warna()
        btMurid.setOnClickListener(this)
        btPaket.setOnClickListener(this)
        btTrx.setOnClickListener(this)
        btBckMain.setOnClickListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.setting,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.menuSet->{
                var init = Intent(this, setting::class.java)
                startActivity(init)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
    fun warna(){

        if (warnakat=="kuning"){
            saya.setBackgroundColor(Color.YELLOW)
        }else if(warnakat=="biru"){
            saya.setBackgroundColor(Color.BLUE)
        }else if (warnakat=="putih"){
            saya.setBackgroundColor(Color.WHITE)
        }else if(warnakat=="hijau"){
            saya.setBackgroundColor(Color.GREEN)
        }

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btMurid->{
                val intent = Intent(this,muridActivity::class.java)
                startActivity(intent)
                finish()
            }
            R.id.btPaket -> {
                val intent = Intent(this,paketActivity::class.java)
                startActivity(intent)
                finish()
            }
            R.id.btTrx -> {
                val intent = Intent(this,transaksiActivity::class.java)
                startActivity(intent)
                finish()
            }
            R.id.btBckMain -> {
                val intent = Intent(this,loginActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }
}
